<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db890');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cNaV(Aj8u;t.%!<t!daep=L|~GyeWbZgOEW|WDbu$OFM8Y8IX!N7JT7.F)b)w$_X');
define('SECURE_AUTH_KEY',  'ULQY`9m_<+GpnmL+eWx7>^hX1=g&U]2ICyZ%/yn4s A;pA|ys@7yT[JpK!UA^)BQ');
define('LOGGED_IN_KEY',    '#^#~`C@JEenzEt7:^Jk(l/D1R(Kp.1n@J+]5+gv_yHA)Aey a%JQ8BN@<u.:9Ca4');
define('NONCE_KEY',        'oBJ6uC)f(Sex^y[ubh)Zp.p&78{LJT(ickE/{>F=$XHUf7VhFMp1KKBB7TXr9Z0j');
define('AUTH_SALT',        'S$lJOk1Xux)HUeX56u=zkF$<(kW|zF YaUW9tr4P|iyl4-33I |zyl92??l:#C]6');
define('SECURE_AUTH_SALT', '(<[G:$]x2uPx2bZk(_>!%Pj3Mq[F7Z>}H^^Mw<G[#yn!pDYhI0N#i c49r$(n8pg');
define('LOGGED_IN_SALT',   '.pn~x$VK7*f,h!@U.lR(q6SX}9=XTn@Sdt@+V$(ym(Hf%m@mP<>.oP99b-S=z?<>');
define('NONCE_SALT',       'qZ7*VmHB:y1M~:s<Vh]wqI=N<k]K3SaKm{>d6$Uupr[X/N:<~*ZTrB&Yd&wu$9|V');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bh_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
