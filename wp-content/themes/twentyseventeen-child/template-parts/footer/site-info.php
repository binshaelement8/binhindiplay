<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<p class="copyright">&copy; 2018. BinHendi Play. All rights reserved.  </p>