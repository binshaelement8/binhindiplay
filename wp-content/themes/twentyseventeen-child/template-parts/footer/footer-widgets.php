<?php
/**
 * Displays footer widgets if assigned
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
			
			
			<div class="footer-top clearfix">
				<div class="container">
					<div class="left">
						<h3 class="mobile-toggle"><?php  echo get_field('footer_title','option') ; ?></h3>
						<p><?php  echo get_field('footer_sub_title','option') ; ?></p>
					</div>
					<div class="right">
						<?php 
							$button_link =   get_field('footer_button_link','option') ; 
							if (!filter_var($button_link, FILTER_VALIDATE_URL)) {	
								$button_link  = home_url(). $button_link;
							}
						?>
						<a href="<?php echo $button_link;?>" class="btn"><?php  echo get_field('footer_button_name','option') ; ?> <i class="fa fa-caret-right"></i></a>
					</div>
				</div>
			</div>


			<div class="footer-bottom clearfix">
				<div class="container">
					<div class="sitemap-widget">
						<h3 class="mobile-toggle">Sitemap</h3>
						<ul>
							<?php 
								$footer_menus = wp_get_nav_menu_items('Footer Menu'); 
								if(!empty($footer_menus)) {
                            foreach ($footer_menus as $key => $value) {
                                echo '<li><a href="'.$value->url.'">'.$value->title.'</a></li>';
                            }
                        }
							?>
							

						</ul>
					</div>


					<div class="what-we-do-widget">
						<h3 class="mobile-toggle">What we do</h3>
						<ul>
							<?php
								 $args = array( 
      
					                'post_type'        => 'services',
					                
					                'post_status'      => 'publish',
					                 'orderby'    => 'menu_order',
					                'sort_order' => 'asc'       
					              
					                );
					        
					      
					          $the_query = new WP_Query( $args );  
					          while ( $the_query->have_posts() ) : $the_query->the_post(); 
							 ?>
							<li><a href="<?php echo home_url().'/what-we-do/'; ?>"><?php the_title();  ?></a></li>
						<?php endwhile;?>
									
						</ul>
					</div>


					<div class="what-we-offer-widget">
						<h3 class="mobile-toggle">What we offer</h3>
						<ul>
							<?php $args = array(      
			              'post_type'        => 'whats_new',        
			              'post_status'      => 'publish',
			              'orderby'    => 'menu_order',
			              'sort_order' => 'asc'       
			            );
			            $the_query = new WP_Query( $args );  
			            if($the_query->have_posts()) : 
			            while ( $the_query->have_posts() ) : $the_query->the_post();   ?>
										<li><a href="<?php echo  home_url() ?>/what-we-offer"><?php the_title();  ?></a></li>
									<?php endwhile;  endif;?>
										
									</ul>
								</div>

					<div class="footer-contact-widget">
						<h3 class="no-arrow">Contact us</h3>
						<?php $contactpage = get_page_by_path( 'contact-us' ); 
						$email ='';$phone ='';
						if(!empty(get_field('email',$contactpage->ID)) ) $email = get_field('email',$contactpage->ID) ;
						if( !empty(get_field('phone',$contactpage->ID)) ) $phone = get_field('phone',$contactpage->ID);
						?>
						<p>
							<span>Email:</span>
							<a href="mailto:<?php  echo $email?>"> <?php  echo $email;?></a>
						</p>
						<p>
							<span>Phone: </span>
							<a href="tel:<?php  echo $phone;?>"><?php  echo $phone;?></a>
						</p>
						<h3 class="mobile-adjust">join our community</h3>
						<ul class="social">
							<?php  $social_links = wp_get_nav_menu_items('Social Links');
                       
                        if(!empty($social_links)) {

                            foreach ($social_links as $key => $value) {
                                $class = isset($value->classes)? $value->classes[0]:'fa-twitter';
                                echo '<li><a href="'.$value->url.'"><i class="fab '.$class.'"></i></a></li>';
                            }
                        } ?>
							
						</ul>
					</div>
					<div class="clr"></div>
					<?php get_template_part( 'template-parts/footer/site', 'info' ); ?>
				</div>
			</div>