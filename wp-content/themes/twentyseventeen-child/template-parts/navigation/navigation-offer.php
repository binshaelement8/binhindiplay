<nav>
          <ul class="secondary-nav">
          	<?php   $urlArr = explode("/", $_SERVER['REQUEST_URI']);  
          		$active_equip = 0 ;
          		if(!in_array('whats-new', $urlArr) && !in_array('equipment', $urlArr) &&  !in_array('colors', $urlArr) && !in_array('useful-documents', $urlArr) 
          			&& !in_array('after-sales-service', $urlArr) ) {
          			$active_equip =1;
          		}

          	 ?>
              <li <?php if(in_array('whats-new', $urlArr)) echo 'class="current-menu-item"';?>><a href="<?php echo site_url(); ?>/whats-new/">What’s new</a></li>
              <li  <?php if(in_array('equipment', $urlArr) || $active_equip ==1) echo 'class="current-menu-item"';?> ><a href="<?php echo site_url(); ?>/document_category/equipment/">Equipment</a></li>
              <li <?php if(in_array('colors', $urlArr)) echo 'class="current-menu-item"';?>><a href="<?php echo site_url(); ?>/document_category/colors/">Colors</a></li>
              <li <?php if(in_array('useful-documents', $urlArr)) echo 'class="current-menu-item"';?>><a href="<?php echo site_url(); ?>/document_category/useful-documents/">Useful Documents</a></li>
              <li <?php if(in_array('after-sales-service', $urlArr)) echo 'class="current-menu-item"';?>><a href="<?php echo site_url(); ?>/document_category/after-sales-service/">After Sales Service</a></li>
          </ul>
        </nav>