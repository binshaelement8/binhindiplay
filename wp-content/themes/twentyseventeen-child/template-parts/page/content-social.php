<div class="instagram-widget clearfix wow fadeInUp" data-wow-duration="1s">
	<div class="container">
		<h3><a href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i> Follow Us</a></h3>
		<ul>
		<?php 

			$userid =   get_field('instagram_user_id','option') ;
            $access_token = get_field('instagram_access_token','option') ;;
            $count = 14;
            $runfile = 'https://api.instagram.com/v1/users/'.$userid.'/media/recent/?access_token='.$access_token.'&count='.$count;          
           
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $runfile);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec ($ch);
            $err = curl_error($ch);
            curl_close ($ch); 
             $result = json_decode($response);   
           
            $instagramPost = $result->data;  
            if(!empty($instagramPost)) {  
					foreach ($instagramPost as  $key => $val) { 	


					 ?>					
						 <li><a data-fancybox="images"   data-caption="<strong><a href='<?php echo  get_field('instagram_profile','option') ?>' target='_blank' style='color: #08b6ed;'><i class='fab fa-instagram'></i>&nbsp;<?php echo $val->user->username; ?>:</a> </strong><?php  echo $val->caption->text; ?>" href="<?php echo  $val->link;?>"><img src="<?php echo $val->images->standard_resolution->url;?>" alt="" width="168" height="168"></a></li>
						 <?php 
					}
				}
		?>
	</ul>
		
	</div>
</div>