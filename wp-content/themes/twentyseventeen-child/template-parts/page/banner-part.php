  <?php 

  $term = get_queried_object();  
  if(!empty($term) &&  (is_category() || is_tax() ) ) { 

   $banner_image =  get_field('banner_image',$term->taxonomy . '_' . $term->term_id); 
   $banner_text =  get_field('banner_text',$term->taxonomy . '_' . $term->term_id); 
   } else {
    $banner_image =  get_field('banner_image',$post->ID); 
     $banner_text = get_field('banner_text',$post->ID);
   } 
  ?>
    <div class="subpage-banner" style="background-image: url(<?php if(!empty($banner_image)) echo $banner_image['url'];  ?>);">
      <div class="container">
        <div class="subpage-banner-content">
        <div class="v-align">
          <h2 class="banner-title wow lightSpeedIn" data-wow-duration="1s"><?php echo $banner_text;?></h2>
        </div>
        </div>
      </div>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/subpage-banner/curv-2.png" alt="">
    </div>
 