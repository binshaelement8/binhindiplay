<?php
/**
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
while ( have_posts() ) : the_post();
?>

<div class="main-banner">

<?php echo do_shortcode( '[rev_slider alias="demu"]' ); ?>


</div>

<div class="banner-bottom clearfix wow bounceInUp" data-wow-duration="1.5s">
	<div class="container">
		<div class="left">
			<h3><?php  echo get_field('footer_title','option') ; ?></h3>
			<p><?php  echo get_field('footer_sub_title','option') ; ?></p>
		</div>
		<div class="right">
			<?php 
							$button_link =   get_field('footer_button_link','option') ; 
							if (!filter_var($button_link, FILTER_VALIDATE_URL)) {	
								$button_link  = home_url(). $button_link;
							}
						?>
			<a href="<?php echo $button_link;?>" class="btn btn-dark"><?php  echo get_field('footer_button_name','option') ; ?><i class="fa fa-caret-right"></i></a>
		</div>
	</div>
</div>


<div class="our-hostory clearfix">
	<div class="container">
		<?php 
			$history = get_field('history'); 
			if(!empty($history)) { 
			$page_data = get_page_by_path($history['read_more_link']);
		?>
		<div class="left">
			<h2><?php  echo $history['sub_heading'];  ?> </h2>
			<p><?php  if(!empty($page_data)) echo wp_trim_words($page_data->post_content,29); ?> </p>
			<a href="<?php  echo  home_url().$history['read_more_link']; ?>">Read More <i class="fa fa-caret-right"></i></a>
		</div>
		<div class="right">
			<img class="wow rubberBand" data-wow-duration="1.5s" src="<?php  echo crop_image($history['image']['url'],805,320); ?>" alt="">
		</div>
	<?php  } ?>
	</div>
</div>

<div class="curv">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/curv.jpg" alt="">
</div>

<div class="what-we-do clearfix">
	<div class="container">
		<?php $what_we_do_part = get_field('what_we_do_part'); 
		if(!empty($what_we_do_part)) {  ?>
		<div class="heading wow fadeInUp" data-wow-duration="1s"> 
			<h2>What We Do</h2>
			<p><?php  if(!empty($what_we_do_part['sub_heading'])) echo $what_we_do_part['sub_heading']; ?></p>
		</div>
		<div class="row wow fadeInUp" data-wow-duration="1s" animation-delay= "0.8s">
			<?php  if(!empty($what_we_do_part['service_part'])) {
					foreach ($what_we_do_part['service_part'] as $key => $eachservice) {
						
						$icon =    get_field('icon_home', $eachservice['services']->ID); 
				  ?>
			<div class="col-6">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>what-we-do" class="each" style="background-image: url(<?php  echo $icon['url'];  ?>);">
					<h3><?php echo $eachservice['services']->post_name; ?></h3>
					<p><?php  echo $eachservice['sub_title']; ?></p>
				</a>
			</div>
		<?php } } ?>
			
		</div>
	<?php } ?>
	</div>
</div>
<div class="our-playgrounds clearfix">
	<div class="container"><?php $what_offer = get_field('offer_part');   ?>
		<?php $play_ground_part = get_field('play_ground_part');  ?>
		<div class="heading wow fadeInLeft" data-wow-duration="1s">
			<h2>Our Playgrounds</h2>
			<p><?php  if(!empty($play_ground_part)) echo $play_ground_part['sub_title']; ?></p>
		</div>

		<div class="playgrounds-carousel owl-carousel wow fadeInRight" data-wow-duration="1s" animation-delay= "0.8s">
			<?php 
					$args = array(      
      					'post_type'        => 'our_playgrouonds',        
        				'post_status'      => 'publish',
        				'posts_per_page'  =>3
          			);

        
   					$playground = new WP_Query( $args );   
   					if(!empty($playground)) {
   						while($playground->have_posts()) : $playground->the_post();
   							$locations = get_the_terms( $post->ID, 'locations' );
   							$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );  
			?>
        	<div class="playground-widget">
				<a href="<?php the_permalink(); ?>"><img src="<?php echo crop_image($large_image_url[0],403,282) ?>" alt=""></a>
				<div class="desc">
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<p><?php  echo get_custom_categories($locations) ?></p>
				</div>
			</div>
		<?php endwhile; } ?>
			
			
		</div>
	</div>
</div>
<div class="what-we-offer clearfix"> 
	<div class="container">
		
		<div class="heading wow fadeInRight" data-wow-duration="1s">

			<h2 style="color:#e14062;">What We Offer</h2>
			
			
			<p><?php  if(!empty($what_offer)) echo $what_offer['sub_heading'];  ?></p>
		</div>
		<div class="what-we-offer-row wow fadeInLeft" data-wow-duration="1s" animation-delay= "0.8s">
			<?php 
				if(!empty($what_offer['offers'])) {
					foreach ($what_offer['offers'] as $key => $eachoffer) {  ?>
						<a href="<?php echo $eachoffer['url']?>" class="each">

							<img src="<?php echo $eachoffer['image']['url'];?>" alt="">
						<h3><?php echo $eachoffer['name']?></h3>

						</a>
						<?php 
						
					}

				}

			?>
			
		</div>
	</div>
</div>

<div class="our-partners clearfix wow fadeInUp" data-wow-duration="1s">
	<div class="container">
		<h2>Our Partners</h2>
         <div class="partners-home">     
        <?php 
        	$args = array(        
                'post_type'        => 'partners',                
                'post_status'      => 'publish',
                 'orderby'    => 'menu_order',
                'sort_order' => 'asc'       
              
                );       
      
          $the_query = new WP_Query( $args );  
           while ( $the_query->have_posts() ) : $the_query->the_post();  
        ?>
          
       		<a href="<?php echo get_field('website',$post->ID); ?>" target="_blank"><img src="<?php echo crop_image(get_the_post_thumbnail_url($post->ID),204,170) ?>" alt=""></a>
			

			<?php  
				endwhile;

			?>
		</div>    
        
        
		
	</div>
</div>

<?php   get_template_part('template-parts/page/content-social'); ?>

<?php endwhile; ?>