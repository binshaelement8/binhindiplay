<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet"> 
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
 <?php $logo=  get_field('logo','option') ;
 if(!empty($logo)) {  ?>
	  <div class="expologo"><img src="<?php  echo crop_image($logo['url'],151,115); ?>" alt=""></div> 
	  <?php } ?>         
	<header class="header clearfix animated fadeInDown" data-wow-duration="">
    
    
    	<div class="container " >
        	<a href="<?php echo site_url(); ?>" class="logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.jpg" alt=""></a>
        	<a href="javascript:void(0);" class="mobile-btn">
        		<span></span>
        		<span></span>
        		<span></span>
        	</a>
        	<span class="clr-mobile"></span>
            <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
        </div>
    </header>   
