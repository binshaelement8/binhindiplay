<?php
/**
  Template Name: Our Playgrounds Single

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();
  $locations = get_the_terms( $post->ID, 'locations' );  
?>

    
    <div class="subpage-banner-2">
      <div class="container">
        <h1 class="banner-title"><?php the_title(); ?></h1>
        <p><?php  echo get_custom_categories($locations) ?></p>
      </div>
    </div>
    <div class="subpage clearfix">
      <div class="container">
      
      <div id="project-gallery">
        <div class="reg-images">
        <?php 
          $gallery_images = get_field('image_gallery');           
            if(!empty($gallery_images)) { 
              foreach ($gallery_images as $key => $image) {                 
                      ?>

                      <a href="<?php echo $image['sizes']['large']; ?>" data-fancybox="gallery">
              <img src="<?php  echo crop_image($image['sizes']['large'],410,345);  ?>" alt="">
            </a>
      <?php          
        } } 
        ?>      
       
      
      
       
      </div>
    </div>
    
    <!-- This page script -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/masonry.pkgd.min.js"></script>
    <script>
      jQuery(window).load(function(){
        jQuery('.op-container').masonry({
          columnWidth: 632,
          gutter: 0,
          itemSelector: '.op-item'
        });
      });
    </script>

<?php

endwhile;

 get_footer();