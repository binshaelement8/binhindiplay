<?php
/**
  Template Name: Useful Documents

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

get_template_part('template-parts/page/banner-part');

// get_template_part('template-parts/page/banner-part');
$term = get_queried_object();  

$childCategory = get_terms( $term->taxonomy, array(
'parent'    => $term->term_id,
'hide_empty' => false
) );

if($term->parent != 0)  { 
  $parent = get_term($term->parent); 
}



?>  
   
    <div class="subpage clearfix">
      <div class="container">
        <div class="text-center">
          <?php  get_template_part('template-parts/navigation/navigation-offer'); ?>
          <div class="heading">
            <h1 class="page-title"><?php if($term->parent == 0)  {  echo $term->name ; } else echo $parent->name;  ?></h1>
            <?php if($term->parent != 0)  {  echo '<h3>'.$term->name.'</h3>';  }    ?>
            <p><?php echo $term->description; ?></p>
          </div>
        </div>
        <?php sort($childCategory);
        if(!empty($childCategory)) {  
        		echo '<ul class="equipment-list wow fadeInUp" data-wow-duration=".8s">';
        		foreach ($childCategory as $key => $category) {
        		
        		  ?>
        		 
        		 	<li><a href="<?php echo get_term_link($category->term_id); ?>"><?php  echo $category->name;?> </a></li>
        		
        <?php 	
        		}
        		echo ' </ul>';

		}else {  ?>
        <ul class="equipment-single-list wow fadeInUp" data-wow-duration=".8s">
         <?php  while ( have_posts() ) : the_post(); 
         $docs =  get_field('document',$post->ID); 
         if(!empty($docs)) { 
         	foreach ($docs as $key => $each_doc) {   ?>
         		 <li>
            <span><?php  echo $each_doc['document_title'];?></span>
            <?php    ?>
            <label for="browse<?php echo $key;?>" class="styled-browse">
              <a id="browse<?php echo $key;?>" href="<?php if($each_doc['file']) {  echo home_url().'/download.php?action=download&id='.$each_doc['file']['ID'].'&post='.$post->ID; } else { echo 'javascript:void(0)' ; } ?>">Browse</a>
            </label>
          
            <div class="clearfix"></div>
          </li>	
         	<?php 
         			} 
         	}
          ?>
         
          <?php endwhile; ?>
        
        </ul>
    <?php } ?>
      </div>
    </div>  


<?php



 get_footer();