<?php
/**
  Template Name: Equipment

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();

?>

    
    <div class="subpage-banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/subpage-banner/2.jpg);">
      <div class="container">
        <div class="subpage-banner-content">
        <div class="v-align">
          <h2 class="banner-title wow lightSpeedIn" data-wow-duration="1s">Learn & Play</h2>
        </div>
        </div>
      </div>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/subpage-banner/curv-2.png" alt="">
    </div>
    <div class="subpage clearfix">
      <div class="container">
        <div class="text-center">
          <nav>
            <ul class="secondary-nav">
              <li><a href="<?php echo site_url(); ?>/whats-new/">What’s new</a></li>
              <li class="current-menu-item"><a href="<?php echo site_url(); ?>/equipment/">Equipment</a></li>
              <li><a href="<?php echo site_url(); ?>/colors/">Colors</a></li>
              <li><a href="<?php echo site_url(); ?>/useful-documents/">Useful Documents</a></li>
              <li><a href="<?php echo site_url(); ?>/after-sales-service/">After Sales Service</a></li>
            </ul>
          </nav>
          <div class="heading">
            <h1 class="page-title">Equipment</h1>
            <p>BinHendi Play is proud to partner with the following companies to design, build and install the playground of your dreams! Each of the buttons below takes you to specific areas of playground equipment. If you would like any of these catalogs mailed to you, please contact us with your request.</p>
          </div>
        </div>
        <ul class="equipment-list wow fadeInUp" data-wow-duration=".8s">
          <?php 

          $metaQuery = array(
              array('taxonomy'   => 'document_category',
              'field' => 'slug',
              'terms'   => 'equipment')
          );
          $args = array(
          'post_type' => 'documents',
          'post_status' => 'publish',
          'tax_query'=>$metaQuery
       
          
      );
            $the_query = new WP_Query( $args );  //  print_r($the_query);
          if( $the_query->have_posts()) :
            while ( $the_query->have_posts()) :  $the_query->the_post();
              ?>
          <li><a href="<?php the_permalink(); ?>"><?php  echo $post->post_title;?> </a></li>
           <?php 
            endwhile;
          endif;
          ?>
        </ul>
      </div>
    </div>
    


<?php

endwhile;

 get_footer();