<?php
/**
  Template Name: Our partners

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post(); 
   $banner_image =  get_field('banner_image',$post->ID);
    get_template_part('template-parts/page/banner-part');
?>

    <div class="subpage clearfix">
      <div class="container">
        <div class="text-center">
        <h1 class="page-title"><?php  echo $post->post_title;;?></h1>
        </div>
        <div class="text-center">
          <ul class="partners-list wow fadeInUp" data-wow-duration=".8s">
            <?php   
            $args = array(        
                'post_type'        => 'partners',                
                'post_status'      => 'publish',
                 'orderby'    => 'menu_order',
                'sort_order' => 'asc'       
              
                );       
      
              $the_query = new WP_Query( $args );  
               while ( $the_query->have_posts() ) : $the_query->the_post(); 
                  $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                  
                

             ?>
            <li>
                <a href="<?php echo get_field('website',$post->ID); ?>" target="_blank"><img src="<?php echo crop_image($large_image_url[0],204,170) ;?>" alt=""></a>
                <p><?php echo $post->post_content; ?> </p>

            </li>           
          
          <?php endwhile; ?>
          </ul>
        </div>
      </div>
    </div>   


<?php

endwhile;

 get_footer();