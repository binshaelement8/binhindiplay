<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();

?>

	<div class="subpage" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shows-bg.jpg);">
	    <div class="container">
	      <h1 class="page-title"><?php the_title(); ?></h1>
	      <br>
	    </div>
	</div>

<?php

endwhile;

 get_footer();
