<?php
/**
  Template Name: Our Playgrounds

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 
$args = array(
        
      
        'post_type'        => 'our_playgrouonds',
        
        'post_status'      => 'publish',
         'orderby'    => 'menu_order',
        'sort_order' => 'asc'   
      
        );
        
      
        $the_query = new WP_Query( $args ); 
         
  

get_template_part('template-parts/page/banner-part');
?>   
 

    <div class="subpage clearfix">
      <div class="container">
        <h1 class="page-title">Our Playgrounds</h1>
        <div class="height40"></div>
        <div class="playgrounds-list">
            <?php 
            $counter = 0; 
           while ( $the_query->have_posts() ) : $the_query->the_post(); 
                $locations = get_the_terms( $post->ID, 'locations' ); 
                $counter++;
                if($counter % 3 == 0) {
                  $class ="fadeInRight";
                }
                else { 
                  $newcheck = $counter+1; 
                    if($newcheck % 3 == 0) {
                    $class ="fadeInUp";
                  }   else {
                   $class ="fadeInLeft";
                  }
                }
             ?>
            <div class="each wow <?php  echo $class;?>" data-wow-duration=".8s">
              <div class="playground-widget"> 
                <a href="<?php the_permalink(); ?>">
                  <img src="<?php the_post_thumbnail_url('havelock-our_playgrouonds-thumbnail' ); ?>" alt=""></a>
                <div class="desc">
                  <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                  <p><?php  echo get_custom_categories($locations) ?></p>
                </div>
              </div>
            </div>
        
            <?php endwhile;  ?>
          </div>
        </div>
      </div>
    


<?php



 get_footer();