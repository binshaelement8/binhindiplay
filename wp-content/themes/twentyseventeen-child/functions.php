<?php

function twentysixteen_scripts() {
	
	// enqueue style
	wp_enqueue_style('font-awesome', get_stylesheet_directory_uri().'/assets/css/fontawesome.css', true, '1.0', 'all' );
	wp_enqueue_style('owl-carousel', get_stylesheet_directory_uri().'/assets/css/owl.carousel.css', true, '1.0', 'all' );
	wp_enqueue_style('fancybox-css', get_stylesheet_directory_uri().'/assets/css/jquery.fancybox.css', true, '1.0', 'all' );
	wp_enqueue_style('animaet-css', get_stylesheet_directory_uri().'/assets/css/animate.css', true, '1.0', 'all' );
	
	// enqueue script
	wp_enqueue_script('parallax-js', get_stylesheet_directory_uri() .'/assets/js/parallax.min.js', array('jquery'), '20150825', true);
	wp_enqueue_script('owl-carousel-js', get_stylesheet_directory_uri() .'/assets/js/owl.carousel.js', array('jquery'), '20150825', true);
	wp_enqueue_script('waypoint-js', get_stylesheet_directory_uri() .'/assets/js/waypoints.js', array('jquery'), '20150825', true);
	wp_enqueue_script('counter-js', get_stylesheet_directory_uri() .'/assets/js/counter.js', array('jquery'), '20150825', true);
	wp_enqueue_script('fancybox-js', get_stylesheet_directory_uri() .'/assets/js/jquery.fancybox.min.js', array('jquery'), '20150825', true);
	wp_enqueue_script('wow-js', get_stylesheet_directory_uri() .'/assets/js/wow.min.js', array('jquery'), '2018', true);
	wp_enqueue_script('custom-js', get_stylesheet_directory_uri() .'/assets/js/custom.js', array('jquery'), '20150825', true);
	
}
add_action('wp_enqueue_scripts', 'twentysixteen_scripts');

function get_custom_categories($cate_arr ) {
	$category_list ="";
	if(!empty($cate_arr)) {
		foreach ($cate_arr as $key => $value) {
			$category_list .= $value->name." ,";
		}
		$category_list = substr($category_list, 0,-1);
	}
	return $category_list;
}

require_once 'aq_resizer.php';



function crop_image($url,$width,$height) { 
	$url = aq_resize($url,$width,$height);
	return $url;
}

add_action('init','download_attchment');
function download_attchment() 
{

	if(isset($_GET['action']) && $_GET['action'] =='download') {
		$attID = $_GET['id'];
		$theFile = wp_get_attachment_url( $attID );   $src= $theFile;
		$theFile   = str_replace(home_url(), "", $theFile);
		
		$file_name = basename( $theFile );
	   	$att_name = $file_name;
	 
	 
	   $file_extension = pathinfo($file_name);  
	   $file_name = basename( $theFile );
	   $att_name = $file_name;
	 
	 
	  $file_extension = pathinfo($file_name);   
	  //security check
	  $fileName = strtolower($file_url); 
	  
	  
	 
	  $file_new_name = $file_name; 
	  $content_type = "";
	  $newfile_name=   $file_new_name;
	  switch( $file_extension['extension'] ) {
			case "png": 
			  $content_type="image/png"; 
			  break;
			case "gif": 
			  $content_type="image/gif"; 
			  break;
			case "tiff": 
			  $content_type="image/tiff"; 
			  break;
			case "jpeg":
			case "jpg": 
			  $content_type="image/jpg"; 
			  break;
			 case "pdf" :
			 $content_type="application/pdf";
			  break;
			 case "doc" :
			 case "docx" :
			 	$content_type="application/octet-stream";
			 break;
			default: 
			  $content_type="application/force-download";
	    }  
		$upload_path = getcwd().$theFile;    		
		if ($fd = fopen ($upload_path, "r")) {  
		   	header('Content-Description: File Transfer');
		    header('Content-Type: '.$content_type);
		  	header('Content-Disposition: attachment; filename="'.$file_new_name.'"');
		    header("Cache-control: private"); //use this to open files directly
			while(!feof($fd)) {
				$buffer = fread($fd, 2048);
				echo $buffer;
			} 
		    header("Cache-control: private");    
		   	flush();       
		    fclose ($fd);
		    exit;

		} else {
			echo 'Error Downloadssssssssssss ';die();
		}
	}
}

if( function_exists('acf_add_options_page') ) {
 
 acf_add_options_page(array(
  'page_title'  => 'Theme General Settings',
  'menu_title' => 'Theme Settings',
  'menu_slug'  => 'theme-general-settings',
  'capability' => 'edit_posts',
 ));
 
 acf_add_options_sub_page(array(
  'page_title'  => 'Theme Header Settings',
  'menu_title'  => 'Header',
  'parent_slug' => 'theme-general-settings',
 ));
 

 acf_add_options_sub_page(array(
  'page_title'  => 'Theme 404 Page Settings',
  'menu_title'  => '404 Page',
  'parent_slug' => 'theme-general-settings',
 ));
 
 acf_add_options_sub_page(array(
  'page_title'  => 'Theme Footer Settings',
  'menu_title'  => 'Footer',
  'parent_slug' => 'theme-general-settings',
 ));
 
}