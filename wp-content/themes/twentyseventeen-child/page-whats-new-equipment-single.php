<?php
/**
  Template Name: Equipment Single

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();
 get_template_part('template-parts/page/banner-part');
?>

    
   
    <div class="subpage clearfix">
      <div class="container">
        <div class="text-center">
          <?php  get_template_part('template-parts/navigation/navigation-offer'); ?>
          <div class="heading">
            <h1 class="page-title">Equipment</h1>
            <p>BinHendi Play is proud to partner with the following companies to design, build and install the playground of your dreams! Each of the buttons below takes you to specific areas of playground equipment. If you would like any of these catalogs mailed to you, please contact us with your request.</p>
          </div>
        </div>
        <ul class="equipment-single-list wow fadeInUp" data-wow-duration=".8s">
        
          <li>
            <span>LSI Catalogue</span>
           <label for="browse" class="styled-browse">
              <a id="browse" href="#.">Browse</a>
            </label>
            <div class="clearfix"></div>
          </li>
          <li>
            <span>LSI Playbook </span>
           <label for="browse" class="styled-browse">
              <a id="browse" href="#.">Browse</a>
            </label>
            <div class="clearfix"></div>
          </li>
          <li>
            <span>PlaySense Preconfigured play equipment </span>
            <label for="browse" class="styled-browse">
              <a id="browse" href="#.">Browse</a>
            </label>
            <div class="clearfix"></div>
          </li>
          <li>
            <span>NottsSports Synthetic Surfacing </span>
            <label for="browse" class="styled-browse">
              <a id="browse" href="#.">Browse</a>
            </label>
            <div class="clearfix"></div>
          </li>
          <li>
            <span>EuroPlay Broc hur</span>
           <label for="browse" class="styled-browse">
              <a id="browse" href="#.">Browse</a>
            </label>
            <div class="clearfix"></div>
          </li>
        </ul>
      </div>
    </div>
    


<?php

endwhile;

 get_footer();