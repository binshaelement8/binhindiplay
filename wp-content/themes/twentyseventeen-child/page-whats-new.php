<?php
/**
  Template Name: What's New

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();

  get_template_part('template-parts/page/banner-part');
?>

 
    <div class="subpage clearfix">
      <div class="container">
        <div class="text-center">
        <?php  get_template_part('template-parts/navigation/navigation-offer'); ?>
        <h1 class="page-title"><?php  echo $post->post_title; ?></h1>
        </div>
        <div class="whats-new row">
          <?php
          $the_query = array();
            $args = array(      
              'post_type'        => 'whats_new',        
              'post_status'      => 'publish',
              'orderby'    => 'menu_order',
              'sort_order' => 'asc'       
            );
            $the_query = new WP_Query( $args );
            $counter = 0;  
            if($the_query->have_posts()) : 
            while ( $the_query->have_posts() ) : $the_query->the_post();   
              $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
              $counter++;
              if($counter % 2 == 0) {
                $class ="fadeInRight";
              }  else {
                 $class ="fadeInLeft";
              }
           ?>
          <div class="col-6 wow <?php  echo $class;?>" data-wow-duration=".8s">
            <img src="<?php echo crop_image($large_image_url[0],589,295) ?>" alt="">
            <h3><?php  the_title(); ?> </h3>
            <p><?php   the_content(); ?></p>
           
            <?php  $gallery_images = array(); 
             $gallery_images = get_field('image_gallery',$post->ID); ?>
           
              <?php
                 if(!empty($gallery_images)) {  ?>
                  <a href="<?php echo crop_image($large_image_url[0],589,295) ?>" class="btn btn-inverse" data-fancybox="gallery">View Gallery</a>
                     <div class="whats-new-gallery">
                       
                  <?php
                    foreach ($gallery_images as $key => $image) {
               ?>
              <img src="<?php echo $image['sizes']['large']; ?>" alt="" data-fancybox="gallery">
             
            <?php } ?>
             </div>
            <?php  } ?>
           
          </div>
        <?php endwhile; endif;?>      
        </div>
      </div>
    </div>
    


<?php

endwhile;

 get_footer();