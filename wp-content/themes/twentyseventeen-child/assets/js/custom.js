jQuery(document).ready(function(e) {
       
	
		
        /* ------------------------------------------------------------------------ */
        /* ANIMATIONS *///
        /* ------------------------------------------------------------------------ */	
         
        /*  if (document.documentElement.clientWidth > 768) {
          
			var fadeDelayAttr;
			var fadeDelay;
			jQuery(".animate-on-load").each(function(){
				if (jQuery(this).data("delay")) {
					
					fadeDelayAttr = jQuery(this).data("delay");
					fadeDelay = fadeDelayAttr;			
				} else {
					fadeDelay = 0;
				}
							
				jQuery(this).delay(fadeDelay).queue(function(){	
					jQuery(this).addClass('animated').clearQueue();
				});			
			});	
			
			
			jQuery('.animate-it').appear();
			jQuery(document.body).on('appear', '.animate-it', function(e, jQueryaffected) {
				var fadeDelayAttr;
				var fadeDelay;
				jQuery(this).each(function(){
					if (jQuery(this).data("delay")) {
						fadeDelayAttr = jQuery(this).data("delay")
						fadeDelay = fadeDelayAttr;				
					} else {
						fadeDelay = 0;
					}			
					jQuery(this).delay(fadeDelay).queue(function(){
						jQuery(this).addClass('animated').clearQueue();
					});			
				})			
			});
            
        }
		*/
		
		

		// === parallax
		jQuery('.parallax-window').parallax();

		if (document.documentElement.clientWidth < 1003) {
			// === nav
			jQuery( ".mobile-btn" ).on( "click", function() {
				jQuery(".main-nav").slideToggle();
				//jQuery(this).toggleClass("nav-active");
			});

			// === footer mobile nav
			jQuery( ".footer .mobile-toggle" ).on( "click", function() {
				jQuery(this).parent().find("ul").slideToggle();
				jQuery(this).toggleClass("active");
			});
		}

		// === Search
		jQuery( ".search-btn" ).on( "click", function() {
			jQuery(".search-widget").fadeToggle(200);
			jQuery(this).toggleClass("search-active");
		});


		// === key sector
		jQuery('.partners-carousel').owlCarousel({
			margin:0,
			loop:false,
			dots:true,
			responsive:{
				0:{
					items:1
				},
				601:{
					items:2
				},
				1100:{
					items:5
				}
			}
		});
		

		// === sectors-carousel
		jQuery('.playgrounds-carousel').owlCarousel({
			margin:20,
			loop:false,
			dots:true,
			nav:false,
			responsive:{
				0:{
					items:1
				},
				500:{
					items:2
				},
				900:{
					items:3
				}
			}
		});

		// === featured projects
		jQuery('.featured-projects-carousel').owlCarousel({
			margin:8,
			loop:false,
			dots:true,
			responsive:{
				0:{
					items:1
				},
				601:{
					items:2
				},
				1100:{
					items:3
				}
			}
		});
		/*jQuery( ".footer h3" ).on( "click", function() {
			jQuery(this).parent().find("ul").slideToggle();
			jQuery(this).toggleClass("active");
		});
		
		
		jQuery('.product-selector-tabs').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
			closed:'accordion'
            
        });
		
		// Slideshow 3
		jQuery("#product-detail-slider").responsiveSlides({
			manualControls: '#product-detail-slider-pager',
			maxwidth: 1000
		});
		
		
		
		jQuery('.scroll').each( function() {
			var $this = jQuery(this), 
				target = this.hash;
				jQuery(this).click(function (e) { 
					e.preventDefault();
					if( $this.length > 0 ) {
						if($this.attr('href') == '#' ) {
							// Do nothing   
						} else {
						   jQuery('html, body').animate({ 
								scrollTop: (jQuery(target).offset().top) - -1
							}, 1000);
						}  
					}
				});
			});
		
		
		
		
		jQuery('.products-carousel').owlCarousel({
			margin:44,
			loop:false,
			dots:true,
			responsive:{
				0:{
					items:1
				},
				601:{
					items:2
				},
				1100:{
					items:3
				}
			}
		});
		
		
		jQuery(".testimonials-carousel").responsiveSlides({
			maxwidth: 1200,
			speed: 800,
			pager: true
		  });
		
		jQuery(".social li:eq(1)").attr('data-delay', '300');
		
		jQuery(".fancybox").fancybox({
			maxWidth	: '90%',
			maxHeight	: '90%',
			fitToView	: true,
			width		: '70%',
			height		: '70%',
			autoSize	: true,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none'
		});
		
		jQuery('.fancybox-media').fancybox({
			openEffect  : 'none',
			closeEffect : 'none',
			width		: '80%',
			height		: '80%',
			type 		: 'iframe',
			helpers : {
				media : {}
			}
		});
		
		*/
        
		new WOW().init();
		
    });
	
	
	
	
	
	jQuery(window).scroll(function() {    
			var scroll = jQuery(window).scrollTop();
		
			if (scroll >= 119) {
				jQuery(".header.clearfix").addClass("fixed", 1000);
			} else {
				jQuery(".header.clearfix").removeClass("fixed", 1000);
			}
		});