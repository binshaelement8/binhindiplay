<?php
/**
  Template Name: Equipment

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();

?>

    
    <div class="subpage-banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/subpage-banner/2.jpg);">
      <div class="container">
        <div class="subpage-banner-content">
        <div class="v-align">
          <h2 class="banner-title">The true object of<br>all human life is play</h2>
        </div>
        </div>
      </div>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/subpage-banner/curv-2.png" alt="">
    </div>
    <div class="subpage clearfix">
      <div class="container">
        <div class="text-center">
          <nav>
            <ul class="secondary-nav">
              <li><a href="">What’s new</a></li>
              <li class="current-menu-item"><a href="">Equipment</a></li>
              <li><a href="">Colors</a></li>
              <li><a href="">Useful Documents</a></li>
              <li><a href="">Warranty</a></li>
            </ul>
          </nav>
          <div class="heading">
            <h1 class="page-title">Equipment</h1>
            <p>BinHendi Play is proud to partner with the following companies to design, build and install the playground of your dreams! Each of the buttons below takes you to specific areas of playground equipment. If you would like any of these catalogs mailed to you, please contact us with your request.</p>
          </div>
        </div>
        <ul class="equipment-list">
          <li><a href="">Site Furnishing </a></li>
          <li><a href="">Shades</a></li>
          <li><a href="">Aquatix</a></li>
          <li><a href="">Musical Instruments</a></li>
          <li><a href="">Fitness Equipment</a></li>
          <li><a href="">NottsSports Surfacing  </a></li>
        </ul>
      </div>
    </div>
    


<?php

endwhile;

 get_footer();