<?php
/**
  Template Name: After Sales Service

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();
 get_template_part('template-parts/page/banner-part');
?>

    
    
    <div class="subpage clearfix">
      <div class="container small">
        <div class="text-center">
           <?php  get_template_part('template-parts/navigation/navigation-offer'); ?>
          <div class="heading">
            <h1 class="page-title">After Sales Service</h1>
          </div>
        </div>
        <ul class="equipment-single-list wow fadeInUp" data-wow-duration=".8s">
           <?php 
          $metaQuery = array(
              array('taxonomy'   => 'document_category',
              'field' => 'slug',
              'terms'   => 'after-sales-service')
          );
          $args = array(
          'post_type' => 'documents',
          'post_status' => 'publish',
          'tax_query'=>$metaQuery
       
          
      );
            $the_query = new WP_Query( $args );  //  print_r($the_query);
          if( $the_query->have_posts()) :
            while ( $the_query->have_posts()) :  $the_query->the_post();
      ?>
          <li>
            <span><?php  echo $post->post_title;?> </span>
           <label for="browse" class="styled-browse">
              <a id="browse" href="#.">Browse</a>
            </label>
            <div class="clearfix"></div>
          </li>
           <?php 
            endwhile;
          endif;
          ?>
         <!--  <li>
            <span>PlaySense Warranty </span>
           <label for="browse" class="styled-browse">
              <a id="browse" href="#.">Browse</a>
            </label>
            <div class="clearfix"></div>
          </li>
          <li>
            <span>Shade Structure Warranty </span>
           <label for="browse" class="styled-browse">
              <a id="browse" href="#.">Browse</a>
            </label>
            <div class="clearfix"></div>
          </li>
          <li>
            <span>Europlay Warranty </span>
           <label for="browse" class="styled-browse">
              <a id="browse" href="#.">Browse</a>
            </label>
            <div class="clearfix"></div>
          </li> -->
        </ul>
      </div>
    </div>
    


<?php

endwhile;

 get_footer();