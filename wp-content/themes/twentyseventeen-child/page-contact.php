<?php
/**
  Template Name: Contact Us

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();

?>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBQ7NCovzNiTQq2Mymj8EhYV2HEKFlB2eQ"></script>
<script type="text/javascript"> 

     google.maps.event.addDomListener(window, 'load', init);
              
            function init() {
              var myLatlng = new google.maps.LatLng(25.157686,55.2308193); //  <-------- Change Location here... -->
                var mapOptions = {            
                    zoom: 13,
          disableDefaultUI: true,
          scrollwheel: false,          
                    center: myLatlng,                                      
                    styles: [
    {
        "featureType": "all",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "weight": "2.00"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#9c9c9c"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#eeeeee"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#7b7b7b"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#c8d7d4"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#070707"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    }
]
                };      
        
                var mapElement = document.getElementById('map');
        
                var map = new google.maps.Map(mapElement, mapOptions);        

        var marker = new google.maps.Marker({
            position: myLatlng,
            animation: google.maps.Animation.DROP,
            map: map,
            icon: '<?php echo get_stylesheet_directory_uri(); ?>/assets/images/marker.png'

          });            
          
          
        function toggleBounce() {

          if (marker.getAnimation() != null) {
          marker.setAnimation(null);
          } else {
          marker.setAnimation(google.maps.Animation.BOUNCE);
          }
        }    
        var mainaddress = '<?php echo   get_field('address1'); ?>' ; 
        var address = '<?php echo   get_field('address2'); ?>' ;
        var email =     '<?php echo   get_field('email'); ?>' ;
        var phone =     '<?php echo   get_field('phone'); ?>' ;
        var fax =     '<?php echo   get_field('fax'); ?>' ;
        var googlemap = '<?php  echo get_field('google_map_url');?>';
        var contentString = '<div id="content-map">'+
          '<div id="siteNotice">'+
          '</div>'+
          '<div id="bodyContent">'+
		  '<div class="leftse"> <h4>BinHendi Play</h4><p>'+mainaddress+'</p><a class="btn btn-inverse" href="'+googlemap+'" target="_blank">Get Directions</a>'+
          '</div>'+
		  '<div class="leftse"><p>'+address+'</p>'+
 		  '<p><strong>'+phone+' <br/>'+email+'</strong></p>'+
          '</div>'+          
          '</div>'+
          '</div>';
      
        var infowindow = new google.maps.InfoWindow({
          content: contentString,
          maxWidth: 450
        });
        
        google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
        });
      }
        </script>


  <div class="map wow fadeInUp" data-wow-duration="1s" id="map"></div>
    
    <div class="subpage clearfix">
      <div class="container">
        
        	<div id="contact-form" class="wow fadeInLeft" data-wow-duration=".8s">
            	<h3>Contact Us</h3>
                <?php echo do_shortcode( '[contact-form-7 id="41" title="Contact form 1"]' ); ?>
                
            </div>
            <div id="contact-address" class="wow fadeInRight" data-wow-duration=".8s">
            	<h5>BinHendi Play</h5>
                <p><?php  echo get_field('address1');?></br><?php  echo get_field('address2');?>
                </p>
                <a class="link" href="callto:<?php  echo get_field('phone');?>">T:&nbsp;&nbsp;&nbsp;&nbsp;<?php  echo get_field('phone');?></a><br />
                <a class="link" href="#.">F:&nbsp;&nbsp;&nbsp;&nbsp;<?php  echo get_field('fax');?></a><br />
                <a class="link" href="<?php  echo get_field('email');?>">E:&nbsp;&nbsp;&nbsp;&nbsp;<?php  echo get_field('email');?></a><br />
                <br />

				<a class="btn btn-inverse" href="<?php  echo get_field('google_map_url');?>" target="_blank">Google Location <i class="fa fa-caret-right"></i></a>
                <a href="" class="btn ">PDF MAP <i class="fa fa-angle-right"></i></a>


                
            </div>
            
            
      </div>
    </div>
    


<?php

endwhile;

 get_footer();