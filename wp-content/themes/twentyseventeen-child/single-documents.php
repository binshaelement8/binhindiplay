<?php
/**
  Template Name: Equipment Single

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();
 //get_template_part('template-parts/page/banner-part');
?>

    
   
    <div class="subpage clearfix">
      <div class="container">
        <div class="text-center">
          <?php  get_template_part('template-parts/navigation/navigation-offer'); ?>
          <div class="heading">
            <h1 class="page-title"><?php  the_title(); ?></h1>
            <p>BinHendi Play is proud to partner with the following companies to design, build and install the playground of your dreams! Each of the buttons below takes you to specific areas of playground equipment. If you would like any of these catalogs mailed to you, please contact us with your request.</p>
          </div>
        </div>
        <ul class="equipment-single-list wow fadeInUp" data-wow-duration=".8s">
          <?php  
           // echo $post->ID;
          $uploads = get_field('document',$post->ID);
         //   print_r($uploads);
            if(!empty( $uploads )) { 
              foreach ($uploads  as $key => $value) {
              
          ?>
          <li>
            <span><?php  echo $value['document_title'];?></span>
           <label for="browse" class="styled-browse">
              <a id="browse" href="#.">Browse</a>
            </label>
            <div class="clearfix"></div>
          </li>
        <?php } } ?>
        
        </ul>
      </div>
    </div>
    


<?php

endwhile;

 get_footer();