<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

				  <div class="subpage clearfix">
		        
		        
		       	<div class="container">
					<section class="error-404 not-found">
						<header class="page-header">
							<span><?php the_field('title','option'); ?></span>
							<h1 class="page-title"></h1>
						</header><!-- .page-header -->
						<div class="page-content">
							<?php _e( the_field('content','option'), 'twentyseventeen' ); ?><br/>

							<a href="<?php the_field('button_link','option'); ?>" class="btn btn-black"><?php the_field('button_name','option'); ?></a>

						</div><!-- .page-content -->
					</section>
		               

		        </div>

		 </div>


<?php
get_footer();
