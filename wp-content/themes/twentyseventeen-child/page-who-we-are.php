<?php
/**
  Template Name: Who we are

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();
    get_template_part('template-parts/page/banner-part');   
?>

    
  
    <div class="subpage clearfix">
      <div class="container">
        <div class="who-we-are-left wow fadeInLeft" data-wow-duration=".8s">
          <h1 class="page-title"><?php echo $post->post_title;?></h1>
          <p><?php echo $post->post_content;?></p>         
          <div class="height50"></div>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>our-playgrouonds" class="btn btn-inverse">Our Playgrounds <i class="fa fa-angle-right"></i></a>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>whats-new" class="btn btn-inverse">What we offer <i class="fa fa-angle-right"></i></a>
        </div>
        <div class="who-we-are-right wow fadeInRight" data-wow-duration=".8s">
          
          <img src="<?php echo crop_image(get_the_post_thumbnail_url($post->ID),582,532) ?>" alt="">
        </div>
      </div>
    </div>
    


<?php

endwhile;

 get_footer();