<?php
/**
  Template Name: What we do

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();
   get_template_part('template-parts/page/banner-part');
?>

    <div class="subpage clearfix">
      <div class="container">
        <h1 class="page-title"><?php  echo $post->post_title;;?></h1>
      </div>
        <div class="what-we-do-row">  
          <div class="each wow fadeInLeft" data-wow-duration=".8s">
            <div class="container">
          <?php 
            $args = array(   
      
                'post_type'        => 'services',
                
                'post_status'      => 'publish',
                 'orderby'    => 'menu_order',
                'sort_order' => 'asc'       
              
                );
        
      
          $the_query = new WP_Query( $args );  
          $key = 0;

         while ( $the_query->have_posts() ) : $the_query->the_post(); 
            $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); //  print_r($large_image_url);
            $icon =    get_field('icon',$post->ID); 
            $image = get_the_post_thumbnail_url($post->ID); 
              if($key % 4 ==0 ) { 
                      echo ' </div></div><div class="each wow fadeInLeft" data-wow-duration=".8s">
                            <div class="container">';
                  }   else if($key %2==0) {
                    echo '</div></div><div class="each wow fadeInRight" data-wow-duration=".8s">
                            <div class="container">';
                  }
                 
                  ?>
              <div class="col-6">
                <img src="<?php echo crop_image($large_image_url[0],588,295) ?>" alt="">
                <h3 style="background-image:url(<?php   if(!empty($icon)) echo $icon['url'];  ?>);"><?php  echo $post->post_title;?></h3>
                <p><?php  echo $post->post_content;?></p>
              </div>
              <?php 
               $key++;
            endwhile;       
            
          ?>       

      </div>
    </div>
    <div class="our-partners clearfix">
      <div class="container">
        <h2>Our Partners</h2>
        <div class="partners-carousel owl-carousel">
          <?php
            $args = array(        
                'post_type'        => 'partners',                
                'post_status'      => 'publish',
                 'orderby'    => 'menu_order',
                'sort_order' => 'asc'       
              
                );       
      
          $the_query = new WP_Query( $args );  
           while ( $the_query->have_posts() ) : $the_query->the_post(); 
            $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
           ?>
          <div class="v-align">
            <span class="helper"></span>
            <img src="<?php echo crop_image($large_image_url[0],102,82) ?>" alt="">
          </div>

        <?php endwhile;?>     
        </div>
      </div>
    </div>


<?php

endwhile;

 get_footer();